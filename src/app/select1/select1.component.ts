import { Component, OnInit } from '@angular/core';
import { ModalService } from '@universis/common';
import { AdvancedSelectComponent } from 'projects/tables/src/public_api';
import { AdvancedSelectService } from 'projects/tables/src/public_api';

@Component({
  selector: 'app-select1',
  templateUrl: './select1.component.html'
})
export class Select1Component implements OnInit {

  public selected: Array<any> = [];

  constructor(private _modal: ModalService, private _selectService: AdvancedSelectService) { }

  ngOnInit() {
  }

  selectStudent() {
    this._selectService.select({
        modalTitle: 'Select student',
        tableConfigSrc: '/assets/lists/Students/select.json'
      }).then((result) => {
        if (result.result === 'ok') {
          this.selected = result.items;
        } else {
          this.selected = [];
        }
      });
  }

}

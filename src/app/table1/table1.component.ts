import { Component, OnInit } from '@angular/core';
import {TABLE1_COFIGURATION} from './table1.configuration';
@Component({
  selector: 'app-table1',
  templateUrl: './table1.component.html'
})
export class Table1Component implements OnInit {

  public tableConfiguration: any = TABLE1_COFIGURATION;
  public filter: any = {};

  constructor() { }

  ngOnInit() {
  }

}

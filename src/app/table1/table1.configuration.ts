export const TABLE1_COFIGURATION = {
  "title": "Students",
  "model": "Students",
  "searchExpression":"indexof(studentIdentifier, '${text}') ge 0 or indexof(person/familyName, '${text}') ge 0 or indexof(person/givenName, '${text}') ge 0 or indexof(user/name, '${text}') ge 0 or indexof(studyProgram/name, '${text}') ge 0 or indexof(studentStatus/name, '${text}') ge 0",
  "selectable": false,
  "multipleSelect": false,
  "columns": [
    {
      "name":"id",
      "property": "id",
      "hidden": true
    },
    {
      "name":"studyProgram",
      "property": "studyProgramId",
      "hidden": true
    },
    {
      "name":"studyProgramSpecialty",
      "property": "studyProgramSpecialtyId",
      "hidden": true
    },
    {
      "name":"studentIdentifier",
      "property": "studentIdentifier",
      "title":"Student identifier"
    },
    {
      "name":"person/familyName",
      "property": "familyName",
      "title":"Name",
      "formatter": "TemplateFormatter",
      "formatString": "${familyName} ${givenName}"
    },
    {
      "name":"person/givenName",
      "property": "givenName",
      "title":"Given Name",
      "hidden": true
    },
    {
      "name":"studyProgram/abbreviation",
      "property": "studyProgram",
      "title":"Study program"
    },
    {
      "name":"studyProgramSpecialty/abbreviation",
      "property": "studyProgramSpecialty",
      "title":"Specialization"
    },
    {
      "name":"semester",
      "property": "semester",
      "title":"Semester"
    },
    {
      "name":"inscriptionYear/name",
      "property": "inscriptionYear",
      "title":"Inscription year"
    },
    {
      "name":"inscriptionPeriod/name",
      "property": "inscriptionPeriod",
      "title":"Inscription period"
    },
    {
      "name":"studentStatus/name",
      "property": "studentStatus",
      "title":"Status"
    }
  ],
  "criteria": [
    {
      "name": "studentIdentifier",
      "filter": "(studentIdentifier eq ${value})",
      "type": "text"
    },
    {
      "name": "studentName",
      "filter": "(indexof(person/familyName, '${value}') ge 0 or indexof(person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "username",
      "filter": "(indexof(user/name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studyProgram",
      "filter": "(studyProgram eq ${value})",
      "type": "text"
    },
    {
      "name": "studentStatus",
      "filter": "(studentStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "inscriptionMode",
      "filter": "(inscriptionMode/id eq '${value}')",
      "type": "text"
    },
    {
      "name": "inscriptionYear",
      "filter": "(inscriptionYear/id eq '${value}')",
      "type": "text"
    },
    {
      "name": "inscriptionPeriod",
      "filter": "(inscriptionPeriod/id eq '${value}')",
      "type": "text"
    },
    {
      "name": "semester",
      "filter": "(semester eq '${value}')",
      "type": "text"
    }
  ],
  "searches": [

  ],
  "defaults":{
    "filter": "department eq '418'"
  },
  "paths" : [

  ]
}

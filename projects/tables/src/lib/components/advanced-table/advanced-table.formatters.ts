import {template, at} from 'lodash';
import {TableColumnConfiguration} from './advanced-table.interfaces';
import {Injector} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import * as ISO6391 from 'iso-639-1/build';
import { ActivatedRoute, Router } from '@angular/router';
import {TemplatePipe} from '@universis/common';

export abstract class AdvancedColumnFormatter implements TableColumnConfiguration {

    protected injector: Injector;
    public className: string;
    public defaultContent: string;
    public formatString: string;
    public formatter: string;
    public hidden: boolean;
    public name: string;
    public order: string;
    public property: string;
    public sortable: boolean;
    public title: string;
    public formatOptions?: any;
    public actions?: Array<any>;

    abstract render(data: any, type: any, row: any, meta: any);
}


export class NestedPropertyFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
            return at(row, column.data.replace(/\//g, '.'));
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class ButtonFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    if (data == null) {
      return '';
    }
    // get column
    const column = meta.settings.aoColumns[meta.col];
    // get format options
    const formatOptions = Object.assign({
      buttonClass: 'btn btn-default'
    }, this.formatOptions);
    // get text content
    let buttonText: string;
    let buttonTitle = '';
    if (formatOptions.buttonText) {
      buttonText = this.injector.get(TranslateService).instant(formatOptions.buttonText);
    } else {
      buttonText = formatOptions.buttonContent;
    }
    if (formatOptions.buttonTitle) {
      buttonTitle = this.injector.get(TranslateService).instant(formatOptions.buttonTitle);
    }
    if (Array.isArray(this.formatOptions.commands)) {
      const activatedRoute: ActivatedRoute = this.injector.get(ActivatedRoute);
      const router: Router = this.injector.get(Router);
      const commands = this.formatOptions.commands.map( command => {
          if (typeof command === 'string') {
              return new TemplatePipe().transform(command, row);
          }
          if (command.outlets) {
              const commandOutlets = {};
              Object.keys(command.outlets).forEach((key) => {
                  if (Object.prototype.hasOwnProperty.call(command.outlets, key)) {
                      const outlet = command.outlets[key];
                      if (Array.isArray(outlet)) {
                          Object.defineProperty(commandOutlets, key, {
                              enumerable: true,
                              configurable: true,
                              writable: true,
                              value: outlet.map((x) => {
                                  return new TemplatePipe().transform(x, row);
                              })
                          });
                      } else {
                          Object.defineProperty(commandOutlets, key, {
                              enumerable: true,
                              configurable: true,
                              writable: true,
                              value: outlet
                          });
                      }
                  }
              });
              return {
                  outlets: commandOutlets
              };
          }
          return command;
      });
      const queryParams = {};
      let replaceUrl = true;
      if (this.formatOptions.navigationExtras && this.formatOptions.navigationExtras.queryParams) {
          Object.keys(this.formatOptions.navigationExtras.queryParams).map( key => {
            queryParams[key] = new TemplatePipe().transform(this.formatOptions.navigationExtras.queryParams[key], row);
          });
      }
      if (this.formatOptions.navigationExtras && typeof this.formatOptions.navigationExtras.replaceUrl !== 'undefined') {
        replaceUrl = this.formatOptions.navigationExtras.replaceUrl;
      }
      let skipLocationChange = false;
        if (this.formatOptions.navigationExtras && typeof this.formatOptions.navigationExtras.skipLocationChange !== 'undefined') {
            skipLocationChange = this.formatOptions.navigationExtras.skipLocationChange;
        }
      const routerLink = router.createUrlTree(commands, {
          relativeTo: activatedRoute,
          queryParams: queryParams,
          replaceUrl: replaceUrl,
          skipLocationChange: skipLocationChange
      }).toString();
      return `<a class="${formatOptions.buttonClass}" title="${buttonTitle}" href="#${routerLink}">${buttonText}</a>`;
    }
    // and return button
    const href = new TemplatePipe().transform(this.formatString, row);
    return `<a class="${formatOptions.buttonClass}" title="${buttonTitle}" href="${href}">${buttonText}</a>`;
  }
  constructor() {
    super();
  }
}

export class LinkFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      return `<a href="${(new TemplatePipe().transform(this.formatString, row))}">${data}</a>`;
    }
    return data;
  }
  constructor() {
    super();
  }
}

export class ActionLinkFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];

    if (column && column.data) {
      this.actions = this.actions || [];

      const allowedActions: any = this.actions.filter(x => {
        if (x.access && x.access.length > 0) {
          let access = x.access;
          access = access.filter(y => {
            if (y.studentStatuses) {
              return y.studentStatuses.find( z => z.alternateName === row.studentStatus);
            }
          });
          if (access && access.length > 0) {
            return x;
          }
        } else {
          return x;
        }
      });

      // return the dropdown menu with 3 dots
      return new TemplatePipe().transform(`<div class='dropdown details-control' data-id='${data}' data-type='student' >
                  <a id="dropdownMenuButton-${data}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa-2x text-dots"></span></a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-${data}">
                     ${allowedActions.map(x => `<a data-name='${x.name}' class="dropdown-item" href="${x.href}">
                         ${this.injector.get(TranslateService).instant(new TemplatePipe().transform(x.title, ({value: data})))}
                        </a>`)
          .join('')}
                    </div>
                </div>`, row);
    }
    return data;
  }

  constructor() {
    super();
  }
}

export class SelectRowFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
          let _tpl = `<input class="checkbox checkbox-theme" type="checkbox"
 id="s-${meta.row}" /><label for="s-${meta.row}">&nbsp;</label>`;
          return  new TemplatePipe().transform(_tpl, row);
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class TemplateFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
          return new TemplatePipe().transform(this.formatString, row);
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class TrueFalseFormatter extends AdvancedColumnFormatter {

    render(data, type, row, meta) {
        return data ? this.injector.get(TranslateService).instant('TrueFalse.long.Yes') :
            this.injector.get(TranslateService).instant('TrueFalse.long.No');
    }

    constructor() {
        super();
    }
}

export class TranslationFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && (data || data === 0)) {
      return this.injector.get(TranslateService).instant(new TemplatePipe().transform(this.formatString, {value: data}));
    }
    return this.injector.get(TranslateService).instant('Unknown');
  }
  constructor() {
    super();
  }
}


export class DateTimeFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      const datePipe: DatePipe = new DatePipe(this.injector.get(TranslateService).currentLang);
      if (typeof data === 'string' && /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}$/.test(data)) {
        return datePipe.transform(new Date(data + ':00'), this.formatString);
      }
      return datePipe.transform(data, this.formatString);
    }
    return data;
  }
  constructor() {
    super();
  }
}

export class NgClassFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      // get formatOptions
      if (this.formatOptions) {
        const ngClass = this.formatOptions.ngClass;
        // check condition for each key
        for (const key of Object.keys(ngClass)) {
          const result = new TemplatePipe().transform(ngClass[key], row);
          const fn = new Function(`return ${result};`);
          // if condition returns true add class
          if (fn() === true) {
            return `<span class="${key}">${data}</span>`;
          }
        }
      }
      return data;
    }
    return data;
  }
  constructor() {
    super();
  }
}
export class LanguageFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
    if (data) {
      return ISO6391.getNativeName(data);
    }
    return data;
  }
  constructor() {
    super();
  }
}


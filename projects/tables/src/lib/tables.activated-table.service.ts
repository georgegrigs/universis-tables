import { Injectable } from '@angular/core';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {AdvancedTableComponent} from './components/advanced-table/advanced-table.component';

@Injectable({
  providedIn: 'root'
})
export class ActivatedTableService {
  private _activeTable: AdvancedTableComponent;
  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) {
    _router.events.forEach((event) => {
      if ( event instanceof NavigationStart) {
        if (this._activeTable) {
          this._activeTable = null;
        }
      }
    });
  }

  get activeTable(): AdvancedTableComponent {
    return this._activeTable;
  }

  set activeTable(tableComponent: AdvancedTableComponent) {
    this._activeTable = tableComponent;
  }
}
